//
//  SearchResultModel.swift
//  idusTest
//
//  Created by hongdae ju on 25/08/2019.
//  Copyright © 2019 hongdae ju. All rights reserved.
//

import UIKit

struct SearchResultModel {
    
    var description: String?        // 설명
    var averageUserRating: CGFloat = 0  // 평점
//    var artistName: String?
    var price: Double = 0           // 가격
    var primaryGenreName: String?   // 카테고리
    var screenshotUrls: [String]?   // 스크린샷
    var sellerName: String?         // 판매자명
    var trackName: String?          // 서비스명
    var trackViewUrl: String?       // 상세보기 url
    var artworkUrl512: String?      // 앱 아이콘
    var trackContentRating: String?  // 등급
    var genres: [String]?           // 카테고리들
    var formattedPrice: String?     // 가격
    var fileSizeBytes: Int64 = 0   // 사이즈
    var version: String?            // 버전
    var releaseNotes: String?       // 릴리즈 노트
    var mDescription: String?       // 설명
    var currency: String?           // 화폐단위
    
    static func create(data: [String: Any]) -> SearchResultModel {
        var model = SearchResultModel()
        
        model.description = data["description"] as? String
        model.averageUserRating = (data["averageUserRating"] as? CGFloat) ?? 0
        model.price = data["price"] as? Double ?? 0
        model.primaryGenreName = data["primaryGenreName"] as? String
        model.screenshotUrls = data["screenshotUrls"] as? [String]
        model.sellerName = data["sellerName"] as? String
        model.trackName = data["trackName"] as? String
        model.trackViewUrl = data["trackViewUrl"] as? String
        model.artworkUrl512 = data["artworkUrl512"] as? String
        model.trackContentRating = data["trackContentRating"] as? String
        model.genres = data["genres"] as? [String]
        model.formattedPrice = data["formattedPrice"] as? String
        let byteString = data["fileSizeBytes"] as? String ?? "0"
        let byte = Int64(byteString) ?? 0
        model.fileSizeBytes = byte
        model.version = data["version"] as? String
        model.releaseNotes = data["releaseNotes"] as? String
        model.mDescription = data["description"] as? String
        model.currency = data["currency"] as? String
        return model
    }
}

//class SearchResultModel: NSObject {
//
//}
