//
//  ExtensionTools.swift
//  idusTest
//
//  Created by hongdae ju on 25/08/2019.
//  Copyright © 2019 hongdae ju. All rights reserved.
//

import UIKit
import Kingfisher

extension UIColor {
    class func RGBA(r: CGFloat, g: CGFloat, b: CGFloat, a: CGFloat) -> UIColor {
        let color = UIColor.init(red: r/255, green: g/255, blue: b/255, alpha: a)
        return color
    }
    
    class func RGB(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return RGBA(r: r, g: g, b: b, a: 1)
    }
}

extension UIImageView {
    func setUrl(string: String?) {
        guard string != nil, let url = URL.init(string: string!) else {
            self.image = nil
            return
        }
        self.kf.setImage(with: url)
    }
}

extension UIView {
    func border(color: UIColor, width: CGFloat) {
        self.layer.borderColor = color.cgColor
        self.layer.borderWidth = width
    }
    
    func round(value: CGFloat) {
        self.layer.masksToBounds = true
        self.layer.cornerRadius = value
    }
}


//class ExtensionTools: NSObject {
//
//}
