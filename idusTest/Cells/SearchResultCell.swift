//
//  SearchResultCell.swift
//  idusTest
//
//  Created by hongdae ju on 25/08/2019.
//  Copyright © 2019 hongdae ju. All rights reserved.
//

import UIKit

class SearchResultCell: UITableViewCell {
    
    @IBOutlet weak var containerView: UIView!
    
    
    @IBOutlet weak var appIconImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var starImageView1: UIImageView!
    @IBOutlet weak var starImageView2: UIImageView!
    @IBOutlet weak var starImageView3: UIImageView!
    @IBOutlet weak var starImageView4: UIImageView!
    @IBOutlet weak var starImageView5: UIImageView!
    
    lazy var starViews: [UIImageView] = {
       return [self.starImageView1, self.starImageView2, self.starImageView3, self.starImageView4, self.starImageView5]
    }()
    
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setStar(rate: CGFloat) {
        for (index, imageView) in self.starViews.enumerated() {
            let floatValue = CGFloat(index) + 1
            if floatValue < rate {
                imageView.image = UIImage.init(named: "starF")
            }else if floatValue-1 < rate ,rate < floatValue {
                imageView.image = UIImage.init(named: "starH")
            }else{
                imageView.image = UIImage.init(named: "starE")
            }
        }
    }

}
