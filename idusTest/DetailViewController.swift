//
//  DetailViewController.swift
//  idusTest
//
//  Created by hongdae ju on 27/08/2019.
//  Copyright © 2019 hongdae ju. All rights reserved.
//

import UIKit
import TagListView
import SafariServices


class DetailViewController: UITableViewController {
    
    var searchResult: SearchResultModel?

    @IBOutlet weak var mCollectionView: UICollectionView!
    @IBOutlet weak var serviceNameLabel: UILabel!
    @IBOutlet weak var sellerNameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    @IBOutlet weak var webButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var contentRatingLabel: UILabel!
    @IBOutlet weak var currentVersionLabel: UILabel!
    
    @IBOutlet weak var arrowImageView: UIImageView!
    @IBOutlet weak var updateInfoLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var tagListView: TagListView!
    
    var isShowReleaseNote: Bool = false {
        didSet{
            if isShowReleaseNote == true {
                self.arrowImageView.image = UIImage.init(named: "arrowUp")
                self.showReleaseNote()
            }else{
                self.arrowImageView.image = UIImage.init(named: "arrowDown")
                self.hideReleaseNoto()
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = false
        self.tableView.separatorColor = UIColor.clear
        
        self.mCollectionView.delegate = self
        self.mCollectionView.dataSource = self
        initViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    private func initViews() {
        guard let searchResult = searchResult else {return}
        
        self.shareButton.border(color: UIColor.RGB(r: 108, g: 108, b: 108), width: 1)
        self.webButton.border(color: UIColor.RGB(r: 108, g: 108, b: 108), width: 1)
        
        self.mCollectionView.reloadData()
        self.serviceNameLabel.text = searchResult.trackName
        self.sellerNameLabel.text = searchResult.sellerName
        self.sizeLabel.text = Units.init(bytes: searchResult.fileSizeBytes).getReadableUnit()
        self.contentRatingLabel.text = searchResult.trackContentRating
        self.currentVersionLabel.text = searchResult.version
        self.descriptionLabel.text = searchResult.mDescription
        
        self.tagListView.alignment = .left
        self.tagListView.removeAllTags()
        self.tagListView.marginX = 15
        self.tagListView.marginY = 15
        for tag in searchResult.genres ?? [] {
            self.tagListView.addTag("#"+tag)
        }
        
        let numberFormatter = NumberFormatter.init()
        numberFormatter.currencyCode = searchResult.currency ?? "USD"
        numberFormatter.numberStyle = .currency
        let price = numberFormatter.string(from: NSNumber(value: searchResult.price)) ?? ""
        self.priceLabel.text = price
        
    }
    
    
    private func gotoWeb() {
        guard let searchResult = searchResult, let url = URL.init(string: searchResult.trackViewUrl ?? "") else {return}
        let viewController = SFSafariViewController(url: url)
//        let viewController = SFSafariViewController(url: URL.init(string: "https://www.google.com")!)
        self.present(viewController, animated: true, completion: nil)
    }
    
    @IBAction func buttonClick(_ button: UIButton) {
        if button == self.webButton {
            self.gotoWeb()
        }else if button == self.shareButton {
            self.share()
        }
    }
    
    
    private func share() {
        
        guard let searchResult = searchResult, let url = searchResult.trackViewUrl else {return}
        
        let alertController = UIAlertController.init(title: "Share", message: nil, preferredStyle: .actionSheet)
        let okAlert = UIAlertAction.init(title: "공유하기", style: .default) { (_) in
            
            let activityViewController = UIActivityViewController.init(activityItems: [url], applicationActivities: nil)
            self.present(activityViewController, animated: true, completion: nil)
            
        }
        let cancelAlert = UIAlertAction.init(title: "취소", style: .cancel) { (_) in
            
        }
        alertController.addAction(okAlert)
        alertController.addAction(cancelAlert)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func showReleaseNote() {
        self.updateInfoLabel.text = searchResult?.releaseNotes ?? ""
        self.reloadReleaseNoteCell()
    }
    
    private func hideReleaseNoto() {
        self.updateInfoLabel.text = nil
        self.reloadReleaseNoteCell()
    }
    
    private func reloadReleaseNoteCell() {
        UIView.setAnimationsEnabled(false)
        UIView.animate(withDuration: 0, animations: {
            let indexPath = IndexPath.init(row: 3, section: 1)
            self.tableView.reloadRows(at: [indexPath], with: .none)
        }) { (_) in
            UIView.setAnimationsEnabled(true)
        }
    }
    
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch (indexPath.section, indexPath.row) {
        case (1,3):
            return isShowReleaseNote == true ? UITableView.automaticDimension : 0
        default:
            return UITableView.automaticDimension
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch (indexPath.section, indexPath.row) {
        case (1,2):
            isShowReleaseNote = !isShowReleaseNote
        default:
            break
        }
    }
}


extension DetailViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.searchResult?.screenshotUrls?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets.init(top: 15, left: 15, bottom: 15, right: 15)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let height = collectionView.frame.size.height - 30
        let width = height * 392 / 696
        return CGSize.init(width: width, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath)
        if let imageView = cell.contentView.viewWithTag(10), let searchResult = searchResult, let screenShots = searchResult.screenshotUrls {
            (imageView as! UIImageView).setUrl(string: screenShots[indexPath.row])
            (imageView as! UIImageView).contentMode = .scaleToFill
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
}
