//
//  ViewController.swift
//  idusTest
//
//  Created by hongdae ju on 25/08/2019.
//  Copyright © 2019 hongdae ju. All rights reserved.
//

import UIKit
import SnapKit


class ViewController: UIViewController {
    
    let keyword = "핸드메이드"
    let headerViewHeight: CGFloat = 60
    let backgroundColor = UIColor.RGB(r: 201, g: 201, b: 201)
    
    @IBOutlet weak var mTableView: UITableView!
    var list: [SearchResultModel] = [SearchResultModel]()
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = keyword
        self.navigationController?.isNavigationBarHidden = true
        self.mTableView.backgroundColor = backgroundColor.withAlphaComponent(0.1)
        
        self.mTableView.delegate = self
        self.mTableView.dataSource = self
        self.mTableView.rowHeight = UITableView.automaticDimension
        self.mTableView.estimatedRowHeight = 600
        self.mTableView.separatorColor = UIColor.clear
        
        loadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func loadData() {
        let session = URLSession.init(configuration: .default)
        
        if var urlComponent = URLComponents.init(string: "https://itunes.apple.com/search") {
            urlComponent.query = "term=" + keyword + "&country=kr&media=software"
            
            guard let url = urlComponent.url else { return }
            
            let task = session.dataTask(with: url) { (data, response, error) in
                if error == nil, let data = data {
                    let json = try! JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
                    print(json)
                    self.list.removeAll()
                    if let result = json["results"] as? [[String: Any]] {
                        for resultData in result {
                            self.list.append(SearchResultModel.create(data: resultData))
                        }
                    }
                    DispatchQueue.main.sync {
                        self.mTableView.reloadData()
                    }
                    
                }else{
                    print(error?.localizedDescription)
                }
            }
            
            task.resume()
        }
    }


}


extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.list.count
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return headerViewHeight
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel.init()
        let headerView = UIView()
        let width = UIScreen.main.bounds.size.width
        headerView.frame = CGRect.init(x: 0, y: 0, width: width, height: headerViewHeight)
        label.text = keyword
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = UIColor.RGB(r: 33, g: 33, b: 33)
        headerView.backgroundColor = UIColor.white
        headerView.addSubview(label)
        
        let separatorView = UIView.init()
        separatorView.backgroundColor = backgroundColor
        headerView.addSubview(separatorView)
        
        label.snp.makeConstraints { (make) in
            make.leading.equalTo(headerView.snp.leading).offset(15)
            make.trailing.equalTo(headerView.snp.trailing).offset(15)
            make.height.equalTo(headerView.snp.height)
            make.centerY.equalTo(headerView.snp.centerY)
        }
        
        separatorView.snp.makeConstraints { (make) in
            make.width.equalTo(headerView.snp.width)
            make.centerX.equalTo(headerView.snp.centerX)
            make.height.equalTo(1)
            make.bottom.equalTo(headerView.snp.bottom)
        }
        
        return headerView
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchResultCell", for: indexPath) as! SearchResultCell
        
        cell.selectionStyle = .none
        
        let model = self.list[indexPath.row]
        cell.appIconImageView.setUrl(string: model.artworkUrl512)
        cell.appNameLabel.text = model.trackName
        cell.sellerNameLabel.text = model.sellerName
        cell.categoryLabel.text = model.primaryGenreName
        cell.priceLabel.text = model.formattedPrice
        cell.setStar(rate: model.averageUserRating)
        
        cell.containerView.border(color: backgroundColor, width: 1)
        cell.containerView.round(value: 4)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let model = self.list[indexPath.row]
        let detailViewController = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DetailViewController") as! DetailViewController
        detailViewController.searchResult = model
        self.navigationController?.pushViewController(detailViewController, animated: true)
    }
    
}

